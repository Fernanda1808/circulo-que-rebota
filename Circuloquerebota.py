from random import randint 
import pygame 
import random 

pygame.init()
swidth=500
sheight=500

screen= pygame.display.set_mode((swidth,sheight))
myclock=pygame.time.Clock()

bg_color=(255,255,255)
ball_color=(255,0,0)
ball_size=15
x=swidth/2
y=sheight/2
movimiento_x="derecha"
movimiento_y="arriba"

while True:
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.quit()
    
    screen.fill(bg_color)
    pygame.draw.circle(screen, ball_color, (x,y), ball_size)
    
    if (x+ball_size)>=500:
        movimiento_x="izquierda"
        
    if (x-ball_size)<=0:
        movimiento_x="derecha"
    
    if (y+ball_size)>=500:
        movimiento_y="abajo"
        
    if (y-ball_size)<=0:
        movimiento_y="arriba"
        
        
    if movimiento_x=="derecha":
     x+=1
    else:
     x-=1
     
    if movimiento_y=="arriba":
        y+=random.randint(1,3)
    else:
        y-=random.randint(1,3)
        
    pygame.display.update()
    myclock.tick(100)
    